include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/pegboard-rail.scad>;

// module rail(height = 1, chamfer = 0, back_wall_gap = default_back_wall_gap, side_wall_gap = 0, copies = 1, mind_the_gap = true, skip = 0, topless = false) {


rail_length = 6;

rotate([0, 180, 0]) translate([0, 0, base_size * -rail_length])
    union() {
        color("skyblue") {
            rail(height = rail_length, chamfer = plate_thickness / 4);
        }
    }
