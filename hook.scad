include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/pegboard-rail.scad>;


module big_hook() {
    intersection() {
        union() {
            
            // horizontal 
            mcube([base_size, base_size * 4, base_size * 1.75], chamfer = base_size / 8, align = [0, 0, 1]);

            // vertical 
            translate([0, 0, base_size]) {
                mcube([base_size, base_size, base_size * 3], chamfer = base_size / 8, align = [0, 4, 1]);
            }

            // outboard 45 degree 
            color("orange") {
                translate([0, base_size * 2, sin(45) * (base_size * 0.875)]) {
                    rotate([45, 0, 0]) {
                        mcube([base_size, base_size * 3, base_size * 1.75], chamfer = base_size / 8, align = [0, 0.5, 0.5]);
                    }
                }
            }
            
            // inboard 45 degree 
            color("salmon") {
                translate([0, 0, base_size]) {
                    rotate([-60, 0, 0]) {
                        mcube([base_size, base_size * 4, base_size * 2], chamfer = base_size / 8, align = [0, 0, 0]);
                    }
                }
            }
        }


            // clipping block
            union() {
                difference() {
                    color("pink") {
                        translate([0, 0, 0]) {
                            mcube([base_size, base_size * 5, base_size * 4], align = [0, 0, 1], chamfer = base_size / 8);
                        }
                    }
                    color("red") {
                    translate([0, 0, 0]) {
                        mcube([base_size * 2, base_size * 4, base_size * 6], align = [0, -1, .9]);
                    }
                }
            }

        }

    }

}

module little_hook() {
    intersection() {
        union() {
            
            // horizontal 
            mcube([base_size, base_size * 2, base_size * 1], chamfer = base_size / 8, align = [0, 0, 1]);

            // vertical 
            translate([0, 0, base_size]) {
                mcube([base_size, base_size, base_size * 1], chamfer = base_size / 8, align = [0, 3, 1]);
            }

            // outboard 45 degree 
            color("orange") {
                translate([0, base_size , sin(45) * (base_size * 0.25)]) {
                    rotate([45, 0, 0]) {
                        mcube([base_size, base_size * 4, base_size * 1], chamfer = base_size / 8, align = [0, 0.5, 0.5]);
                    }
                }
            }
            
            // inboard 45 degree 
            color("salmon") {
                translate([0, 0, base_size * 1.75]) {
                    rotate([-60, 0, 0]) {
                        mcube([base_size, base_size * 4, base_size * 1], chamfer = base_size / 8, align = [0, 0, -1]);
                    }
                }
            }
        }



        // clipping block
            union() {
                difference() {
                    color("pink") {
                        translate([0, 0, 0]) {
                            mcube([base_size, base_size * 4, base_size * 2], align = [0, 0, 1], chamfer = base_size / 8);
                        }
                    }
                    color("red") {
                    translate([0, base_size / 16, 0]) {
                        mcube([base_size * 2, base_size * 4, base_size * 6], align = [0, -1, .9]);
                    }
                }
            }

        }

    }


}



if (false) {

    little_hook();

    translate([-75, 0, 0]) {
        big_hook();
    }

}