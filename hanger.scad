include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/pegboard-rail.scad>;

module finger(girth = base_size / 2) {

    module cubey() {
        mcube([girth, girth, girth], align = [0, 1, 1], chamfer = girth / 4);
    }


    module floor() {
        color("lightgreen") {
            intersection() {
                mcube([girth, girth * 2, girth * 2], align = [0, 1, 1]);
                translate([0, -1 * (girth - (girth / 2)), 0]) {
                    hull() union() {
                        cubey();
                        translate([0, girth, 0]) {
                            cubey();
                        }
                        translate([0, 0, girth / 2]) {
                            cubey();
                            translate([0, girth, 0]) {
                                cubey();
                            }
                        }
                    }
                }
            }
        }
    }    
    
    
    module arm() {
        color("lightblue") {
            hull() union() {
            translate([0, girth - (girth / 2), 0]) {
                    cubey();
                    translate([0, girth, girth]) {
                        cubey();
                    }
                }
            }
        }
    }
    
    module wall() {
        color("orange") {
            hull() union() {
                translate([0, girth * 2 - (girth / 2), girth]) {
                    cubey();
                    translate([0, 0, girth]) {
                        cubey();
                    }
                }
            }
        }
    }

    floor();
    arm();
    wall();


}


module hook() {
    finger();
    color("blue") {
        rail(height = 2, chamfer = wall_thickness / 4);
    }
}

// hook();
