include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/pegboard-rail.scad>;
include <hook.scad>;


// module rail(height = 1, chamfer = 0, back_wall_gap = default_back_wall_gap, side_wall_gap = 0, copies = 1, mind_the_gap = true, skip = 0, topless = false) {


rail_height = 4;
rail_width = 2;

//up = true;
up = false;



color("skyblue") {
    rotate([0, up ? 180 : 0, 0]) {
        translate([0, 0, (up ? -1 : 0) * (base_size * rail_height)]) {
            rail(height = rail_height, copies = rail_width, chamfer = plate_thickness / 4);
        }
    }
}

big_hook();



